# Hastily #

Get/fix subtitles when you are in a haste to watch a movie!

Hastily is a Rust program that can search, download, analyze and sync subtitles.

# Installation #

You need a program called [Cargo](http://doc.crates.io/) to compile Hastily from source. Once you have installed it, just go to the root folder of this repo, and issue the cargo build --release command. The resulting binary will be in the target/release folder.

# Using #

## Search subtitles. ##

Suppose you want to get the subtitles for the movie "Ferris Bueller's Day Off". You invoke Hastily as shown below.

    ./hastily getsub ferris

This sends the program to OMDB's movie search service, and search there for movie names that contain the word "ferris".
It will then lists all the movies that was found. Like this,

    1. Ferris Bueller's Day Off (tt0091042)
    2. Ferris Bueller (tt0098795)
    3. The Night Ferris Bueller Died (tt0240760)
    4. Inside Story: Ferris Bueller's Day Off (tt2150301)
    5. The Ferris Wheel (tt2207641)
    6. Farewell, Ferris Wheel (tt2349677)
    7. Ferris Wheel at Night (tt2496624)
    8. Ferris Wheel on Fire (tt2991876)
    9. Brad Ferris' Game Plan (tt3464692)
    10. When the Ferris Wheel Stops (tt4158616)
    Please select a number between 1 and 10..

Now just select the required movie from this list, in this case it is the first one, so enter "1" and press enter.

Now the program will go to opensubtitles.org and fetch all the available English subtitles for the movie and will save
them in the "subtitles" folder. The actual subtitles will be available in the "subtitles/subtitles" folder and the downloaded archives will be in "subtitles/archives".

## Finding the right subtitle. ##

Ok. Now you have all the available subtitles in your 'subtitles/subtitles' folder. How will you find the right one for your movie file? It is not very fun to try each of them in your video player until you find the right one. Hastily can help you here. It has a "report" command, that accept a file system path as an argument, and will print a report after analyzing all the .srt files found there.

This report will consist of selecting a candidate dialog from the subtitle (which is a unique, non repeating dialog that is present in all the available subtitle files), and printing out the time at which that dialog appears in each of the subtitle files. You can use it to match the correct subtitle for the movie file, by looking up that dialog in the movies video stream and matching the time it appears in it with the times printed out by the report.

The following output shows the report for the subtitles downloaded for the movie "Take Shelter".

    8 subtitles in final set
    -------------------------
    Candidate dialog:
    -------------------------
    You laugh. But this
    little one in your arms
    -------------------------
    Time offsets for the candidate dialog:
    -------------------------
    00:04:12,669 --> 00:04:15,092 | subtitles/subtitles/sub_11.zip/Take.Shelter.2011.LIMITED.720p.BluRay.X264-AMIABLE.English.srt
    00:04:12,685 --> 00:04:15,097 | subtitles/subtitles/sub_12.zip/Take Shelter (2011).srt
    00:04:12,685 --> 00:04:15,097 | subtitles/subtitles/sub_13.zip/Take Shelter - 2011 DVDRIP XVID-WBZ.srt
    00:04:12,685 --> 00:04:15,097 | subtitles/subtitles/sub_16.zip/Take.Shelter.English.srt
    00:04:12,685 --> 00:04:15,097 | subtitles/subtitles/sub_17.zip/Take.Shelter.English.srt
    00:04:12,703 --> 00:04:15,116 | subtitles/subtitles/sub_3.zip/Take Shelter.2011.DVDRIP XVID-WBZ.srt
    00:04:12,966 --> 00:04:15,366 | subtitles/subtitles/sub_5.zip/takeShelter-faye-xvid.srt
    00:04:12,685 --> 00:04:15,097 | subtitles/subtitles/sub_6.zip/Take.Shelter.2011.LiMiTED.DvDRip.XviD.Ac3.Feel-Free.srt

## Fixing/Syncing subtitles. ##

Sometimes, there just ain't no perfect sub for your movie, release out there. In those cases you will have to work with whatever you can get. Sometimes, you will get a subtitle made for a different release. Sometimes you can use it after
you sync it once in your player, by delaying/advancing the subtitles by a certain amount. But often, the frame rate will
also differ, which causes the subtitles to go out of sync after a while.

Hastily can fix the subtitle taking this into account. You just need to find two dialogs in the movie, and tell Hastily when they appear in the movie. By using this information, Hastily can calculate the correct time for the rest of the subtitles and can make a perfectly synced subtitle with these timings.

The command that does syncing is as follows.

    ./hastily sync 2 00:00:10,100 45 00:00:40,000 fixed.srt

First two arguments to the sync command are the index of the first dialog and the time at which it appear in the movie. The next two arguments are the index of the second and its time. The last argument is the file name where you want to save the synced subtitle.

[Here](http://z-petal.com/subfixer) is a web interface to this program where you can interactively do this syncing.
You can start this locally by using the "web" command of Hastily.
    
    ./hastily web