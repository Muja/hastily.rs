use std::io::{Error, ErrorKind};
use std::io::Result as IOResult;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use core::convert;
use std::fmt::Formatter;
use url::ParseError;
use hyper::error::Result as HResult;
use hyper::error::Error as HError;
use rustc_serialize::json::DecoderError;
use std::path::PathBuf;

pub fn make_error<T>(err: String) -> HastilyResult<T> {
    return Err(HastilyError { error: err });
}

pub struct HastilyError {
    pub error: String,
}

impl Display for HastilyError {
    fn fmt(&self, formatter: &mut Formatter) -> Result<(), FmtError> {
        format!("{}", self.error);
        return Ok(());
    }
}

impl convert::From<String> for HastilyError {
    fn from(s: String) -> HastilyError {
        return HastilyError { error: s };
    }
}

impl convert::From<Error> for HastilyError {
    fn from(e: Error) -> HastilyError {
        return HastilyError { error: "Error!".to_string() };
    }
}

impl convert::From<ParseError> for HastilyError {
    fn from(e: ParseError) -> HastilyError {
        return HastilyError { error: "Error!".to_string() };
    }
}

impl convert::From<DecoderError> for HastilyError {
    fn from(e: DecoderError) -> HastilyError {
        return HastilyError { error: "Error!".to_string() };
    }
}

impl convert::From<HError> for HastilyError {
    fn from(e: HError) -> HastilyError {
        return HastilyError { error: "Error!".to_string() };
    }
}

pub type HastilyResult<T> = Result<T, HastilyError>;
