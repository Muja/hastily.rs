use hyper::header::Connection;
use hyper::header::Headers;
use hyper::header::{Host, UserAgent, Referer};
use hyper::client::RedirectPolicy::FollowNone;
use hyper::Client;
use hyper::error::Result as HResult;
use hyper::client::response::Response;
use hyper::Url;
use std::io::Read;
use std::io::Write;
use std::io::Result as IOResult;
use std::fs::File;
use std::io::{Error, ErrorKind};
use core::convert::From;

use std::path::PathBuf;
use std::io::BufReader;
use xml::reader::{EventReader, XmlEvent};

use errors::{self, HastilyResult};

pub fn get_subtitles(imdb: &String,
                     language: &String,
                     dst: &PathBuf)
                     -> HastilyResult<Vec<PathBuf>> {
    let url = make_query(language, imdb).expect("Cannot make url for fetching subtitles");
    let response = try!(get_content_at_url(&url, None));
    let links = get_links(&get_body(response));
    if links.len() == 0 {
        return Err(errors::HastilyError {
            error: format!("No {} subtitles found for the movie {}", language, imdb),
        });
    } else {
        return download_links(links, dst);
    }
}

fn download_links(links: Vec<String>, dst: &PathBuf) -> HastilyResult<Vec<PathBuf>> {
    let mut files: Vec<PathBuf> = Vec::new();
    for (i, link) in links.iter().enumerate() {
        println!("Downloadin {}", link);
        let mut file_name = dst.clone();
        file_name.push(format!("sub_{}.zip", i + 1));
        let file = match File::create(&file_name) {
           Ok(f) => f,
           _ => {
               continue;
           }
        };
        if get_opensubtitles_archive(link, file).is_err() {
           println!("Cannot fetch subtitle at {}", link);
        }
        files.push(file_name);
    }
    Ok(files)
}

fn get_body(mut response: Response) -> String {
    let mut body = String::new();
    response.read_to_string(&mut body).unwrap();
    body
}

fn get_binary_body(mut response: Response) -> HastilyResult<Vec<u8>> {
    let mut body: Vec<u8> = Vec::new();
    try!(response.read_to_end(&mut body));
    return Ok(body);
}

fn get_opensubtitles_archive(link: &String, file: File) -> HastilyResult<()> {
    let url = try!(Url::parse(link));
    let response = try!(get_content_at_url(&url, None));
    let ru = try!(get_header(response, "Location".to_string())
                      .ok_or("Failed to get referer url.".to_string()));
    let referer_url = try!(Url::parse(&ru));

    let download_link = try!(make_link_url_from_ad_link(link)
                                 .ok_or("Cannot make download link from ad link".to_string()));
    return download_opensubtitles_archive(&download_link, &referer_url, file);
}

fn make_link_url_from_ad_link(ad_link: &String) -> Option<Url> {
    match Url::parse(&ad_link.replace("/subad/", "/sub/")) {
        Ok(url) => Some(url),
        Err(_) => None,
    }
}

fn download_opensubtitles_archive(url: &Url, referer: &Url, mut file: File) -> HastilyResult<()> {
    let mut headers = make_default_headers();
    println!("{}", referer.serialize());
    headers.set(Referer(referer.serialize()));
    headers.set(Host {
        hostname: "dl.opensubtitles.org".to_string(),
        port: None,
    });
    let response = try!(get_content_at_url(url, Some(headers)));
    let body = try!(get_binary_body(response));
    try!(file.write_all(&body[..]));
    return Ok(());
}


fn make_default_headers() -> Headers {
    let mut h = Headers::new();
    h.set(UserAgent("Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:43.0) Gecko/20100101 \
                     Firefox/43.0"
                        .to_string()));

    return h;
}

fn get_header(response: Response, header_name: String) -> Option<String> {
    for header in response.headers.iter() {
        if header.name().to_lowercase() == header_name.to_lowercase() {
            return Some(header.value_string());
        }
    }
    return None;
}

fn get_content_at_url(url: &Url, headers: Option<Headers>) -> HResult<Response> {
    let mut client = Client::new();
    client.set_redirect_policy(FollowNone);
    match headers {
        Some(headers) => {
            println!("Sending request to {} with headers!", url.serialize());
            client.get(url.clone()).header(Connection::close()).headers(headers).send()
        }
        None => client.get(url.clone()).header(Connection::close()).send(),
    }
}

fn get_links(xml: &String) -> Vec<String> {
    println!("{}", xml);
    let mut result: Vec<String> = vec![];
    for e in EventReader::new(BufReader::new(xml.as_bytes())).into_iter().filter(|x| x.is_ok()) {
        match e.unwrap() {
            XmlEvent::StartElement { name, attributes: attr, .. } => {
                if name.local_name == "IDSubtitle" {
                    for a in attr {
                        if a.name.local_name == "LinkDownload" {
                            result.push(a.value);
                        }
                    }
                }
            }
            _ => {}
        }
    }
    return result;
}

fn make_query(language_id: &String, imdb_id: &String) -> Option<Url> {
    let search_url = format!("http://www.opensubtitles.\
                              org/en/search/sublanguageid-{}/imdbid-{}/offset-{}/xml",
                             language_id,
                             imdb_id,
                             "0");
    match Url::parse(&search_url) {
        Ok(omdb_url) => {
            return Some(omdb_url);
        }
        Err(_) => {
            return None;
        }
    }
}
