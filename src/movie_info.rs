use movie_info_sources;
use errors::{self, HastilyResult};

#[derive(Debug, Clone)]
pub struct MovieInfo {
    pub title: String,
    pub year: String,
    pub movie_type: String,
    pub imdb: String,
}

pub fn search_slug(movie_name_slug: String) -> HastilyResult<Vec<MovieInfo>> {
    return movie_info_sources::omdb::search_slug(movie_name_slug);
}
