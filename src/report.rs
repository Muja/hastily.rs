use std::path::PathBuf;
use std::fs::read_dir;
use std::fs::DirEntry;
use std::fs::File;
use std::io::Read;
use std::collections::HashSet;

use parsers::srt::{Srt, Dialog, parse};

pub fn get_srts(src: PathBuf) -> Vec<Box<Srt>> {
    let mut srts: Vec<Box<Srt>> = Vec::new();
    for fle in get_all_files(src) {
        match fle.clone().extension() {
            Some(ext) => {
                if ext.to_str().unwrap() == "srt" {
                    let srt = parse_srt_file(fle).unwrap();
                    srts.push(Box::new(srt));
                }
            }
            None => {
                continue;
            }
        }
    }
    return srts;
}

pub fn parse_srt_file(fle: PathBuf) -> Option<Srt> {
    let mut f = File::open(fle.clone()).unwrap();
    let mut content_bin: Vec<u8> = Vec::new();
    f.read_to_end(&mut content_bin);
    remove_bom(&mut content_bin);
    let content = String::from_utf8_lossy(&content_bin[..]).into_owned();
    match parse(content) {
        Some(mut srt) => {
            srt.filename = Some(fle.clone());
            return Some(srt);
        },
        None => {
            return None;
        }
    }
}

fn remove_bom(content_bin: &mut Vec<u8>) {
    if content_bin.starts_with(&[0xEF, 0xBB, 0xBF]) {
        println!("{}", "Removing bom");
        content_bin.remove(0);
        content_bin.remove(0);
        content_bin.remove(0);
    }
}

pub fn make_report(srts: Vec<Box<Srt>>) {
    let (subtitles, dialogs) = get_filtered_set(srts);
    println!("{} subtitles in final set", subtitles.len());
    println!("-------------------------");
    println!("Candidate dialog:");
    println!("-------------------------");
    let candidate_dialog = get_candidate_dialog(&dialogs).unwrap();
    for line in &candidate_dialog.dialogs {
        println!("{}", line);
    }
    println!("-------------------------");
    println!("Time offsets for the candidate dialog:");
    println!("-------------------------");
    for boxed_sub in &subtitles {
        for dialog in &boxed_sub.subtitles {
            if candidate_dialog == *dialog {
                println!("{} | {}", dialog.time.clone(), boxed_sub.filename.clone().unwrap().to_str().unwrap());
            }
        }
    }
    println!("-------------------------");
}

fn get_candidate_dialog(dialogs: &HashSet<Dialog>) -> Option<Dialog> {
    let ordered_dialogs: Vec<Dialog> = order_dialog_hashset(dialogs);
    let cutoff = (ordered_dialogs.len() as f64 * 0.05) as i64;
    for dialog in ordered_dialogs {
        if dialog.len() > 30 && dialog.index > cutoff && !dialog.contains_any(&vec!("Advertise")) {
            return Some(dialog.clone());
        }
    }
    return None;
}

pub fn order_dialog_hashset(dialogs: &HashSet<Dialog>) -> Vec<Dialog> {
    let mut ordered_dialogs: Vec<Dialog> = Vec::new();
    for dialog in dialogs {
        ordered_dialogs.push(dialog.clone());
    }
    ordered_dialogs.sort_by_key(|a| a.index.clone());
    return ordered_dialogs;
}

fn get_filtered_set(srts: Vec<Box<Srt>>) -> (Vec<Box<Srt>>, HashSet<Dialog>) {
    let mut filtered_set: Vec<Box<Srt>> = Vec::new();
    let mut hashed_tuples = make_hashed_tuples(srts);
    let mut maybe_result: Option<HashSet<Dialog>> = None;
    for (ref srt, ref dialog_set) in hashed_tuples {
        let file_path = srt.filename.clone().unwrap();
        let file_name = file_path.to_str().unwrap();
        if file_name.contains("cd") {
            println!("Discarding file {} because multi cd", file_name);
            continue;
        }
        match maybe_result.clone() {
            None => {
                maybe_result = Some(dialog_set.clone());
                filtered_set.push(srt.clone());
            }
            Some(ref result) => {
                let im_result:HashSet<Dialog> = result.intersection(dialog_set).cloned().collect();
                let size_diff = result.len() - im_result.len();
                if  size_diff < 500  {
                    maybe_result = Some(im_result);
                    filtered_set.push(srt.clone());
                } else {
                    println!("Discarding file {} because size diff {}, current size {}", srt.filename.clone().unwrap().to_str().unwrap(), size_diff, result.len());
                }
            }
        }
    }
    return (filtered_set, maybe_result.unwrap());
}

fn make_hashed_tuples(srts: Vec<Box<Srt>>) -> Vec<(Box <Srt>, HashSet<Dialog>)> {
    let mut ret: Vec<(Box<Srt>, HashSet<Dialog>)> = Vec::new();
    for srt in srts {
        let srt_clone = srt.clone();
        ret.push((srt, make_dialog_hash_set(&srt_clone.subtitles)));
    }
    return ret;
}

pub fn make_dialog_hash_set(dialogs: &Vec<Dialog>) -> HashSet<Dialog> {
    let mut hs: HashSet<Dialog> = HashSet::new();
    let mut duplicates = HashSet::new();
    for d in dialogs {
        if hs.remove(d) == false && !duplicates.contains(d) {
            hs.insert(d.clone().to_owned());
        } else {
            duplicates.insert(d.clone().to_owned());
        }
    }
    return hs;
}

fn get_all_files(src: PathBuf) -> Vec<PathBuf> {
    let mut result: Vec<PathBuf> = Vec::new();
    for fp in read_dir(src).unwrap() {
        let entry_path = fp.unwrap().path();
        if entry_path.is_dir() {
            for ic in get_all_files(entry_path) {
                result.push(ic);
            }
        } else {
            result.push(entry_path);
        }
    }
    return result;
}
