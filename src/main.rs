extern crate hyper;
extern crate rustc_serialize;
extern crate core;
extern crate xml;
extern crate zip;
extern crate url;
extern crate regex;
extern crate multipart;
#[macro_use]
extern crate horrorshow;

use hyper::Server;
use hyper::server::Request;
use hyper::server::Response;
use hyper::header::{ContentType, ContentDisposition, Pragma};
use hyper::header::{Headers, DispositionType, DispositionParam, Charset};
use hyper::mime;

use std::io::prelude::*;
use std::path::PathBuf;
use std::time::Duration;
use std::env;

mod ui;
mod movie_info_sources;
mod parsers;
mod subtitle_sources;
mod movie_info;
mod extractors;
mod errors;
mod report;
mod sync;
mod templates;

use std::io::{Error, ErrorKind};
use subtitle_sources::opensubtitles;
use std::io::Result as IOResult;
use std::fs::{DirBuilder, copy};
use std::env::current_dir;
use std::fs::{File, remove_file};
use std::io::BufReader;
use errors::HastilyResult;
use std::collections::HashMap;
use std::collections::HashSet;
use multipart::server::Multipart;
use multipart::server::MultipartData;

fn get_anchors(pairs: &HashMap<String, String>) -> Option<((String, i64), (String, i64))> {
    let mut index_set: HashSet<i64> = HashSet::new();
    for (name, value) in pairs {
	match value.parse::<i64>() {
		Ok(_) => {
			let components: Vec<&str> = name.split('_').collect();
			if components.len() == 2 {
			    if let Ok(index) = components[1].parse() {
				if components[0] == "hr" {
				    index_set.insert(index);
				}
			    }
			}
		},
		_ => {
		}
	}
    }

    let mut index_vec: Vec<i64> = Vec::new();
    for index in index_set {
        index_vec.push(index.clone());
    }

    if index_vec.len() == 2 {
        index_vec.sort();
        let start_anchor_index = index_vec[0].clone();
        let end_anchor_index = index_vec[1].clone();
        
        match (
            pairs.get(&format!("hr_{}", start_anchor_index)),
            pairs.get(&format!("mn_{}", start_anchor_index)),
            pairs.get(&format!("sec_{}", start_anchor_index)),
            pairs.get(&format!("msec_{}", start_anchor_index)),
            pairs.get(&format!("hr_{}", end_anchor_index)),
            pairs.get(&format!("mn_{}", end_anchor_index)),
            pairs.get(&format!("sec_{}", end_anchor_index)),
            pairs.get(&format!("msec_{}", end_anchor_index))
            ) {
            (Some(hr1), Some(mn1), Some(sec1), Some(msec1), Some(hr2), Some(mn2), Some(sec2), Some(msec2)) => {
                let sa_time = format!("{:0>#02}:{:0>#02}:{:0>#02},{:0>#03}", hr1, mn1, sec1, msec1);
                let ea_time = format!("{:0>#02}:{:0>#02}:{:0>#02},{:0>#03}", hr2, mn2, sec2, msec2);
                return Some(((sa_time, start_anchor_index), (ea_time, end_anchor_index)));
            },
            _ => {
		println!("Didnt find all parts!");
                return None;
            }
        };
    } else {
        return None;
    }

}

fn send_html_res(mut res: Response, content: &[u8]) {
    let mime: mime::Mime = "text/html;charset=UTF-8".parse().unwrap();
    res.headers_mut().set(ContentType(mime));
    res.send(content);
}

fn handler(mut request: Request, mut res: Response) -> () {
    let mut pairs: HashMap<String, String> = HashMap::new();
    let mut error: Option<String> = None;
    match Multipart::from_request(request) {
        Ok(mut multipart) => {
            loop {
                match multipart.read_entry() {
                    Ok(Some(field)) => {
                        match field.data {
                            MultipartData::Text(value) => {
                                if value != "" {
                                    pairs.insert(field.name, value.to_string());
                                }
                            }
                            MultipartData::File(mut mFile) => {
                                match mFile.save_in_limited("uploaded_files", 1048576) {
                                    Ok(saved_file) => {
                                        match make_form(saved_file.path.clone(), mFile.filename()) {
                                            Some(form) =>  {
                                                send_html_res(res, form.as_bytes());
                                                return ();
                                            },
                                            None => {
                                                println!("{}", "Cannot make form");
                                                remove_file(saved_file.path.clone());
                                            }
                                        };
                                    }, 
                                    Err(err) => {
                                        println!("{}", err);
                                    }
                                }
                            }
                        }
                    },
                    _ => {
                        break;
                    }
                }
            }
        },
        Err(r) => {
        }
    }

    match (pairs.get("srt_file_name"), pairs.get("original_name")) {
        (Some(file_name), Some(original_name)) => {
            match get_anchors(&pairs) {
                Some(((first_anchor, first_index), (second_anchor, second_index))) => {
                    let mut source = PathBuf::from("uploaded_files");
                    let upload_folder = source.clone();
                    source.push(file_name);
                    match source.canonicalize() {
                        Ok(source_can) => {
                            if source_can.parent().unwrap() == upload_folder.canonicalize().unwrap() {
                                match sync(source.clone(), &first_index, &first_anchor, &second_index, &second_anchor) {
                                    Some((srt, Some(m), Some((o, sgn)))) => {

                                        let mime: mime::Mime = "text/srt;charset=utf-8".parse().unwrap();
                                        res.headers_mut().set(ContentType(mime));
                                        res.headers_mut().set(ContentDisposition {
                                            disposition: DispositionType::Attachment,
                                            parameters: vec![DispositionParam::Filename(
                                              Charset::Iso_8859_1, 
                                              None, 
                                              format!("{}.synced.{}x{}{}.srt", original_name, m, sgn, parsers::srt::convert_duration_to_string(o)).as_bytes().to_vec()
                                            )]
                                        });
                                        res.headers_mut().set(Pragma::NoCache);
                                        res.send(format!("{}", srt).as_bytes());
                                        remove_file(source);
                                        return ();
                                    }, 
                                    _ => {
                                        remove_file(source);
                                        error = Some("Something went wrong! Please check your inputs.".to_string());
                                    }
                                }
                            }
                        },
                        _ => {
                            error = Some("Source file not found! Try uploading it once again..".to_string());
                        }
                    }
                    
                },
                None => {
                    error = Some("Something went wrong! Please check your inputs.".to_string());
                }
            };
        },
        _ => {
        }
    };

    send_html_res(res, templates::index(error).as_bytes());
}

fn main() {
    let mut args = env::args();
    match args.nth(1) {
        Some(command) => {
            if command == "getsub" {
                match args.nth(0) {
                    Some(movie_name_slug) => {
                        getsub(movie_name_slug);
                    },
                    None => {
                        panic!("Require a movie name or part of it!");
                    }
                }
            } else if command == "report" {
                match args.nth(0) {
                    Some(source_path) => {
                        let srts = report::get_srts(PathBuf::from(source_path));
                        report::make_report(srts);
                    },
                    None => {
                        panic!("Require path to a directory that contains srt files, one per top level folder.");
                    }
                }
            } else if command == "sync" {
                match args.nth(0) {
                    Some(source_path) => {
                        let first_index:i64 = args.nth(0).unwrap().parse().unwrap();
                        let first_ts:String = args.nth(0).unwrap();
                        let second_index:i64 = args.nth(0).unwrap().parse().unwrap();
                        let second_ts:String = args.nth(0).unwrap();
                        if let Some((srt, Some(m), Some((o, sgn)))) = sync
                            (PathBuf::from(source_path), 
                            &first_index, &first_ts,
                            &second_index, &second_ts) {
                                let mut target = File::create(PathBuf::from(&args.nth(0).unwrap())).unwrap();
                                target.write_all(format!("{}", srt).as_bytes());
                        }
                    },
                    None => {
                        panic!("Require path to an srt file.");
                    }
                }
            } else if command == "search" {
                match args.nth(0) {
                    Some(source_path) => {
                        match args.nth(0) {
                            Some(ref search_str) => {
                                println!("{}", search(PathBuf::from(source_path), search_str));
                            },
                            None => {}
                        };
                    },
                    None => {
                        panic!("Require path to an srt file.");
                    }
                }
            } else if command == "web" {
                Server::http("0.0.0.0:3000").unwrap()
                    .handle(handler).unwrap(); 
            }
        },
        None => {
        }
    }
}


fn sync(path: PathBuf, int_index_1: &i64, timestamp_1: &str, int_index_2: &i64, timestamp_2: &str) -> Option<(parsers::srt::Srt, Option<f64>, Option<(Duration, sync::Sign)>)> {
    let mut srt = report::parse_srt_file(path).unwrap();
    let hashed_dialogs = report::make_dialog_hash_set(&srt.subtitles);
    let mut anchor_dialog_1: Option<parsers::srt::Dialog> = None;
    let mut anchor_dialog_2: Option<parsers::srt::Dialog> = None;
    let mut flag:usize = 0;
    for dialog in &hashed_dialogs {
        if dialog.index == int_index_1.clone() {
            anchor_dialog_1 = Some(dialog.clone());
            flag += 1;
        }
        if dialog.index == int_index_2.clone() {
            anchor_dialog_2 = Some(dialog.clone());
            flag += 1;
        }
        if flag == 2 {
            break;
        }
    }

    let timestamp_1_full: Result<parsers::srt::DialogTime, _> = format!("{} --> {}", timestamp_1.clone(), timestamp_1.clone()).parse();
    let timestamp_2_full: Result<parsers::srt::DialogTime, _> = format!("{} --> {}", timestamp_2.clone(), timestamp_2.clone()).parse();

    match (anchor_dialog_1, anchor_dialog_2, timestamp_1_full, timestamp_2_full) {
        (Some(mut ad_1), Some(mut ad_2), Ok(ts_1), Ok(ts_2)) => {
            if ts_1.start_time > ts_2.start_time {
                return None;
            }
            ad_1.time = ts_1;
            ad_2.time = ts_2;
            let (multiplier, offset) = sync::fix(&mut srt, ad_1, ad_2);
            return Some((srt, multiplier, offset));
        },
        _ => {
            return None;
        }
    };
}

fn make_form(path: PathBuf, filename: Option<&str>) -> Option<String> {
    match report::parse_srt_file(path.clone()) {
        Some(srt) => {
            let hashed_dialogs = report::make_dialog_hash_set(&srt.subtitles);
            let ordered_dialogs = report::order_dialog_hashset(&hashed_dialogs);
            let mut template_fields: Vec<(i64, Vec<String>, Vec<String>)> = Vec::new();
            for dialog in ordered_dialogs {
                let components = parsers::srt::convert_duration_to_components(dialog.time.start_time.clone());
                template_fields.push((dialog.index, components, dialog.dialogs));
            }
            return Some(templates::form_page(templates::srt_fields(path.file_name().unwrap().to_str().unwrap().to_string(), filename.unwrap().to_string(), template_fields)));
        },
        _ => {
            return None;
        }
    }
}

fn search(path: PathBuf, search: &str) -> String {
    let srt = report::parse_srt_file(path).unwrap();
    let hashed_dialogs = report::make_dialog_hash_set(&srt.subtitles);
    let ordered_dialogs = report::order_dialog_hashset(&hashed_dialogs);
    let targets = vec!(search);
    let mut ret = String::new();
    for dialog in ordered_dialogs {
        if dialog.contains_any(&targets) {
            ret.push_str(&format!("{}", dialog));
        }
    }
    return ret;
}

fn getsub(movie_name_slug: String) {
    match movie_info::search_slug(movie_name_slug) {
        Ok(movie_options) => {
            match ui::resolve_movie_name(&movie_options) {
                Some(movie_info) => {
                    println!("Getting subtitles for {}", &movie_info.imdb);
                    let dst = PathBuf::from("subtitles/archives");
                    let archives = match get_subtitles(&movie_info, &dst) {
                        Ok(archives) => archives,
                        Err(err) => panic!(err.error),
                    };
                    let source_dir = PathBuf::from("subtitles/subtitles");
                    match extract_files(archives, &source_dir) {
                        Ok(extracted_pbs) => {
                            for pb in extracted_pbs {
                                println!("{}", pb.to_str().unwrap());
                            }
                        }
                        Err(err) => {
                            println!("{}", err);
                        }
                    }
                }
                None => {
                    println!("No results!");
                }
            };
        }
        Err(_) => {
            println!("No results!");
        }
    };
}

fn get_subtitles(movie_info: &movie_info::MovieInfo, dst: &PathBuf) -> HastilyResult<Vec<PathBuf>> {
    try!(make_container_dir());
    return opensubtitles::get_subtitles(&movie_info.imdb, &"eng".to_string(), &dst);
}

fn extract_files(src_files: Vec<PathBuf>, dst: &PathBuf) -> HastilyResult<Vec<PathBuf>> {
    let mut extracted_files: Vec<PathBuf> = Vec::new();
    for archive_path in src_files {
        let mut extract_dst = dst.clone();
        let file_name = match archive_path.file_name() {
            Some(file_name) => file_name,
            None => continue,
        };
        extract_dst.push(file_name);
        match DirBuilder::new().create(&extract_dst) {
            Ok(_) => {}
            _ => {
                println!("Cannot create dir for extraction");
                continue;
            }
        };
        println!("Extracting {}", archive_path.to_str().unwrap());
        match extractors::zip::extract(&archive_path, &extract_dst) {
            Ok(pbs) => {
                for pb in pbs {
                    extracted_files.push(pb);
                }
            }
            Err(err) => {
                println!("{}", err.error);
            }
        };
    }
    return Ok(extracted_files);
}

fn make_container_dir() -> HastilyResult<()> {
    let mut builder = DirBuilder::new();
    builder.recursive(true);
    try!(builder.create("subtitles/archives"));
    try!(builder.create("subtitles/subtitles"));
    return Ok(());
}
