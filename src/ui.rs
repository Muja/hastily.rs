use std::io;
use core::num::ParseIntError;
use movie_info::MovieInfo;
use std::path::PathBuf;
use parsers::srt::{Srt, Dialog};
use std::collections::HashSet;

pub fn resolve_movie_name<'a>(options: &'a Vec<MovieInfo>) -> Option<&'a MovieInfo> {
    for (index, option) in options.iter().enumerate() {
        println!("{}. {} ({})", index + 1, option.title, option.imdb);
    }
    match get_selection(options.len()) {
        Some(selection) => {
            return Some(&options[selection - 1]);
        }
        None => {
            return None;
        }
    }
}

fn get_selection(max: usize) -> Option<usize> {
    println!("Please select a number between 1 and {}..", max);
    let mut buffer = String::new();
    match io::stdin().read_line(&mut buffer) {
        Ok(_) => {
            buffer.pop();
            let input: Result<usize, ParseIntError> = buffer.parse();
            match input {
                Ok(selection) => {
                    if (selection < 1) || (selection > max) {
                        return get_selection(max);
                    }
                    return Some(selection);
                }
                Err(_) => {
                    return get_selection(max);
                }
            };
        }
        Err(_) => {
            return None;
        }
    };
}
